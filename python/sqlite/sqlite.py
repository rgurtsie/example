import sqlite3 as sq


# Команды для БД системы
# SELECT col1, col2 ... FROM <table_name> --- вывод данных бд
# INSERT INTO <table_name> (<column_name1>, <column_name2>, ...) VALUES (<value1>,<value2>, ...) --- добавим данные в уровень 



# 1)

#нижняя кострукция автономно закрыввате контекст при ошибке
with sq.connect("saper.db") as con:
   
   cur = con.cursor() # возвращает класс cursor
   
   # cur.execute("DROP TABLE users") #удаляем данные из бд
   # cur.execute("DROP TABLE IF EXISTS users") #удаляем данные из бд
   # cur.execute("CREATE TABLE users(name TEXT, sex TEXT, old INTEGER, score INTEGER)") # создаем бд
   # cur.execute("CREATE TABLE IF NOT EXISTS users(name TEXT, sex INTEGER, old INTEGER, score INTEGER)") # добавили условие при создании бд
   
   # cur.execute("INSERT INTO users VALUES('Михаил', 'man', 15, 1000)") # добавляем данные в table
   # cur.execute("INSERT INTO users VALUES('Flex', 'bi', 24, 6000)") # добавляем данные в table
   # cur.execute("INSERT INTO users VALUES('Михаил', 'man', 15, 1000)") # добавляем данные в table
   # cur.execute("INSERT INTO users VALUES('Глория', 'woman', 25, 1000)") # добавляем данные в table
       
   cur.execute("SELECT rowid,* FROM users") # подготавливает данные для вывода
   
   # data =cur.fetchall() # возвращает данные в row
   # print(data)
   for result in cur: # возварщает данные
      print(result)
   
   
   # 2)
   
   # print("divider")
   
   # cur.execute("UPDATE users SET score=500 WHERE name LIKE 'Г_%'") # обновляет данные в бд с определенным условием
   # cur.execute("UPDATE users SET score=500 WHERE rowid=1") # обновляет данные в бд
   
   # cur.execute("DELETE FROM  users")
   # cur.execute("SELECT rowid, * FROM users") # подготавливает данные для вывода
   
   # for result in cur: # возварщает данные
   #    print(result)


# 3) группировки данных в таблица

# count() -- подсчет чисел записей
# sum() -- подсчет суммы чисел записей
# arv() -- подсчет среднего арифм. чисел записей
# min() -- подсчет наимеьшего значения чисел записей
# min() -- подсчет наибольшего значения чисел записей

   # cur.execute("SELECT sum(score) FROM users WHERE name='Михаил'") # суммирование 

   # count = cur.execute("SELECT count (score) as co FROM users WHERE name='Михаил'") # выводит количество с опред. условием
   # cur.execute("SELECT sum(score) FROM users WHERE name='Михаил'") # суммирование 

   # cur.execute("SELECT name, sum(score) as sum FROM users  GROUP BY name")
   # data=cur.fetchall()
   # print(data)
   # for result in cur:
   #    print(result)
   
   
   
# 4) оператор JOIN для формирования сводного отчета

# {
   # Из users выбирают name, sex, и добавляют в games на место user_id и просматривая соответствие rowid 
   cur.execute("SELECT name, sex, games.score FROM games")
   cur.execute("JOIN users ON games.user_id=users.rowid")
# }

   data=cur.fetchall()
   print(data)
   