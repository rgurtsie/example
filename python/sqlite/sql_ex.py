import sqlite3 as sq

with sq.connect("saper.db") as con:
   cur = con.cursor()
   
   # cur.execute("CREATE TABLE games(user_id INTEGER, score INTEGER, time INTEGER)")
   
   # cur.execute("INSERT INTO games VALUES(1, 1000, 6)") # добавляем данные в table
   # cur.execute("INSERT INTO games VALUES(2, 6000, 12)") # добавляем данные в table
   # cur.execute("INSERT INTO games VALUES(3, 3000, 8)") # добавляем данные в table   
   
   # cur.execute("DELETE FROM  games")     
   cur.execute("SELECT * FROM games")
   
   for result in cur:
      print(result)
      
   print("divider")   
      
with sq.connect("saper.db") as con:
   cur = con.cursor()
   
   # cur.execute("CREATE TABLE users(name TEXT, sex INTEGER, old INTEGER, score INTEGER)")
   
   # cur.execute("INSERT INTO users VALUES('Михаил', 1, 25, 1000)") # добавляем данные в table
   # cur.execute("INSERT INTO users VALUES('Яна', 2, 19, 6000)") # добавляем данные в table
   # cur.execute("INSERT INTO users VALUES('Давид', 1,20, 3000)") # добавляем данные в table   
   
   # cur.execute("DELETE FROM  users")     
   cur.execute("SELECT * FROM users")
   
   for result in cur:
      print(result)      