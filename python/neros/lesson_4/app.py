from flask import Flask, request, jsonify
import numpy as np
import tensorflow as tf


app = Flask(__name__)

model = tf.keras.models.load_model('model.h5')

@app.route('/predict', methods=['POST'])
def predict():
    data = request.json  # Получить входные данные из POST-запроса
    # Преобразовать данные в формат, необходимый для модели
    input_data = np.array(data['input'])
    # Предсказать значения с помощью модели
    predictions = model.predict(input_data)
    # Вернуть предсказанные значения в формате JSON
    return jsonify(predictions.tolist())


if __name__ == '__main__':
    app.run()
