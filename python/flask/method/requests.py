from flask import Flask, jsonify, request

app = Flask(__name__)

languages=[{'name' : 'JavaScript'},{'name' : 'Python'},{'name' : 'Dart'}]
data={"message" : "It works!"}



@app.route("/",methods=['GET'])
def test():
   return jsonify(data)

@app.route('/lang',methods=['GET'])
def get_all():
   return jsonify({"languages": languages})

@app.route('/lang/<string:name>',methods=['GET'])
def getOne(name):
  lang=[language for language in languages if language['name']==name ]
  print(lang[0])
  return jsonify({'language':lang})

@app.route("/lang",methods=['POST'])
def addOne():
   language={'name' : request.json['name']}
   
   languages.append(language)
   return jsonify({"languages":languages})


@app.route("/lang",methods=['POST'])
def addTwo():
   language={'name' : request.json['name']}
   
   languages.append(language)
   return jsonify({"languages":languages})




@app.route("/lang/<string:name>", methods=['PUT'])
def update_data(name):
   langs=[language for language in languages if language['name']==name]
   langs[0]['name'] = request.json['name']
   

   return jsonify({"language" : langs[0]})

@app.route("/lang/<string:name>",methods=['DELETE'])
def delete_data(name):
   langs=[language for language in languages if language['name']==name]
   languages.remove(langs[0]) 
   
   return jsonify({"languages" : languages })

   

if __name__ == '__main__':
   app.run(debug=True, port=8000)
