import flask
from flask import Flask, render_template,make_response,url_for,request,jsonify
from flask import Response
from flask_restful import Api, Resource

import os
import sqlite3



app = Flask(__name__)
api=Api(app)

class Helloword(Resource):
    def get(self):
        return {'Hello Word'}
    
    
api.add_resource(Helloword, "/word")    
        

# @app.route('/')
# def index():
#     return "Home"


   
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=int('5000'), debug=True)



