import flask
from flask import Flask
from flask import Response
import cx_Oracle
connection = cx_Oracle.connect('atlas_dbmon2_r', 'DFjklwiu234$', 'INT8R')

app = Flask(__name__)


@app.route('/')
@app.route('/home')
def index():
    return "Home"

@app.route('/get_test', methods=['GET'])
def get_test():
    response = flask.jsonify({"test": "test responce from API"})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/get_schemas/<db_name>', methods = ['GET'])
def get_schemas(db_name):
    query = f"select * from table(atlas_dbmon.dbmon_pkg.get_all_schemas({db_name}))"
    cursor = connection.cursor()
    cursor.execute(query)
    res = cursor.fetchone()
    response = flask.jsonify({"all_shemas": res})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response



@app.route('/get_blocking_sessions/<db_name>/<from_date>/<to_date>', methods = ['GET'])
def get_schemas(db_name, from_date, to_date):
    query = f"select * from table(atlas_dbmon.dbmon_pkg.get_blocking_sessions({db_name, from_date, to_date}))"
    cursor = connection.cursor()
    cursor.execute(query)
    res = cursor.fetchone()
    response = flask.jsonify({"sessions": res})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/get_blocking_sessions/<db_name>/<from_date>/<to_date>', methods = ['GET'])
def get_schemas(db_name, from_date, to_date):
    query = f"select * from table(atlas_dbmon.dbmon_pkg.get_blocking_sessions({db_name, from_date, to_date}))"
    cursor = connection.cursor()
    cursor.execute(query)
    res = cursor.fetchone()
    response = flask.jsonify({"sessions": res})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route('/get_basic_metrics/<db_name>/<mins>', methods = ['GET'])
def get_schemas(db_name, mins):
    query = f"select * from table(atlas_dbmon.dbmon_pkg.get_basic_metrics({db_name, mins}))"
    cursor = connection.cursor()
    cursor.execute(query)
    res = cursor.fetchone()
    response = flask.jsonify({"metrics": res})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/get_db_up_status/<db_name>', methods = ['GET'])
def get_schemas(db_name):
    query = f"select * from table(atlas_dbmon.dbmon_pkg.get_basic_metrics({db_name}))"
    cursor = connection.cursor()
    cursor.execute(query)
    res = cursor.fetchone()
    response = flask.jsonify({"status": res})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/get_exp_plan_data/<sql_id>/<db_name>', methods = ['GET'])
def get_schemas(sql_id, db_name):
    query = f"select * from table(atlas_dbmon.dbmon_pkg.get_exp_plan_data({sql_id, db_name}))"
    cursor = connection.cursor()
    cursor.execute(query)
    res = cursor.fetchone()
    response = flask.jsonify({"plan_data": res})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/get_jobs_info/<db_name>/<schema_name>', methods = ['GET'])
def get_schemas(db_name, schema_name):
    query = f"select * from table(atlas_dbmon.dbmon_pkg.get_jobs_info({db_name, schema_name}))"
    cursor = connection.cursor()
    cursor.execute(query)
    res = cursor.fetchone()
    response = flask.jsonify({"plan_data": res})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/get_apply_lag/<db_name>', methods=['GET'])
def get_apply_lag(db_name):
    query = f"select * from table(atlas_dbmon.dbmon_pkg.get_apply_lag({db_name}))"
    cursor = connection.cursor()
    cursor.execute(query)
    res = cursor.fetchone()
    return res


@app.route('/get_awr_stats4_sqlid/<db_name>/<sql_id>', methods=['GET'])
def get_awr_stats4_sqlid(db_name, sql_id):
    query = f"select * from table(atlas_dbmon.dbmon_pkg.get_awr_stats4_sqlid{db_name, sql_id}"
    cursor = connection.cursor()
    cursor.execure(query)
    res = cursor.fethone()
    return res

@app.route('/get_account_info/<db_name>/<schema_name>', methods = ['GET'])
def get_schemas(db_name, schema_name):
    query = f"select * from table(atlas_dbmon.dbmon_pkg.get_account_info({schema_name, db_name}))"
    cursor = connection.cursor()
    cursor.execute(query)
    res = cursor.fetchone()
    response = flask.jsonify({"name": res})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/get_schema_sess_details/<db_name>/<user_name>', methods = ['GET'])
def get_schemas(db_name, user_name):
    query = f"select * from table(atlas_dbmon.dbmon_pkg.get_schema_sess_details({db_name, user_name}))"
    cursor = connection.cursor()
    cursor.execute(query)
    res = cursor.fetchone()
    response = flask.jsonify({"details": res})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

   
@app.route('/get_sess_distribution_func/<db_name>', methods = ['GET'])
def get_schemas(db_name):
    query = f"select * from table(atlas_dbmon.dbmon_pkg.get_sess_distribution_func({db_name}))"
    cursor = connection.cursor()
    cursor.execute(query)
    res = cursor.fetchone()
    response = flask.jsonify({"data": res})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/get_compact_sess_distribution/<db_name>', methods = ['GET'])
def get_schemas(db_name):
    query = f"select * from table(atlas_dbmon.dbmon_pkg.get_compact_sess_distribution({db_name}))"
    cursor = connection.cursor()
    cursor.execute(query)
    res = cursor.fetchone()
    response = flask.jsonify({"data": res})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/get_num_sessions_chart/<db_name>/<schema_name>/<from_date>/<to_date>/<node>', methods = ['GET'])
def get_schemas(db_name, schema_name, from_date, to_date, node):
    query = f"select * from table(atlas_dbmon.dbmon_pkg.get_num_sessions_chart({db_name, schema_name, from_date, to_date, node}))"
    cursor = connection.cursor()
    cursor.execute(query)
    res = cursor.fetchone()
    response = flask.jsonify({"data": res})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response
    

@app.route('/get_storage_data/<db_name>/<schema_name>/<selected_year>', methods = ['GET'])
def get_schemas(db_name, schema_name, selected_year):
    query = f"select * from table(atlas_dbmon.dbmon_pkg.get_storage_data({db_name, schema_name, selected_year}))"
    cursor = connection.cursor()
    cursor.execute(query)
    res = cursor.fetchone()
    response = flask.jsonify({"data": res})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response
    
    
@app.route('/about')
def about():
    return "About"


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=int('5000'), debug=True)




