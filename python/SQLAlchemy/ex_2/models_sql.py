from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column,Integer, Float, String, DateTime, BigInteger, SmallInteger, Boolean
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.dialects.postgresql import JSONB
import datetime, json, random

DeclBase = declarative_base()

class User(DeclBase):
   __tablename__ = "users"
   id = Column(Integer, primary_key=True)
   telegram_id = Column(BigInteger, index=True)
   keys_limit = Column(Integer, default=3)
   referral = Column(String)
   country = Column(String)
   created = Column(DateTime, default=datetime.datetime.utcnow)
   data = Column(JSONB, default={})
   
class LastAction(DeclBase):
   __tablename__ = "last_actions"
   id = Column(Integer, primary_key=True)
   name = Column(String)
   telegram_id = Column(BigInteger, index=True)
   created = Column(DateTime, default=datetime.datetime.utcnow)
   data = Column(JSONB, default={})
   
   
   
   