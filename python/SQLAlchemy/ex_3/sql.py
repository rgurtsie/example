from flask import Flask
from datetime import datetime
from flask_sqlalchemy import  SQLAlchemy
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:1234@localhost/sampledb'
db = SQLAlchemy(app)

Session = sessionmaker(bind=engine)

class Users(db.Model):
   id = db.Column(db.Integer, primary_key=True)
   email = db.Column(db.String(50), unique=True) # является уникальным
   psw = db.Column(db.String(500), nullable=True)
   date = db.Column(db.DateTime, default=datetime.utcnow)
   
   def __repr__(self):
      return f"<users {self.id}>"
   
class Profiles(db.Model):
   id = db.Column(db.Integer, primary_key=True)
   name = db.Column(db.String(50), nullable=True)
   old = db.Column(db.Integer)
   city = db.Column(db.String(100))
   
   user_id = db.Column(db.Integer, db.ForeignKey("users.id"))  # connect to user
   
   def __repr__(self):
      return f"<profiles {self.id}>"   

if __name__ == "__main__":
   app.run(debug=True)

