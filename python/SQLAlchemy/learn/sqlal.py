from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from flask import Flask

# создает таблицу
engine = create_engine('postgresql://postgres:1234@localhost/flask', echo=False)

Session = sessionmaker(bind=engine)
session = Session()

Base = declarative_base()

class Student(Base):
   __tablename__ ="student"
   
   id = Column(Integer, primary_key=True)
   name = Column(String(50))
   age = Column(Integer)
   grade =Column(String(50))
   
Base.metadata.create_all(engine) 
  
# добавляет данные в эту таблицу

# student_1 = Student(name="Jhon", age=27, grade="Fifth")
# student_2 = Student(name="Anya", age=30, grade="Fourth")
# # session.add(student_1)
# session.add_all([student_1, student_2])
# session.commit()

# как читать data

# students = session.query(Student)

# for student in students:
#    print(student.name, student.age, student.grade)
   
# update data
# student = session.query(Student).filter(Student.name=="Jhon").first()
# student.name = "Kevin"
# session.commit()

# delete data
student = session.query(Student).filter(Student.name=="Kevin").first()
session.delete(student)
session.commit()